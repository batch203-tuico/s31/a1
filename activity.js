/* 
    ACTIVITY: 
    1. In the S31 folder, create an a1 folder and an activity.js file inside of it.
    2. Copy the questions provided by your instructor into the activity.js file.


    3. The questions are as follows:
    - What directive is used by Node.js in loading the modules it needs?
        => Node.js uses the "require" directive in loading the modules it needs.


    - What Node.js module contains a method for server creation?
        => The "http" module contains a method for server creation.

    - What is the method of the http object responsible for creating a server using Node.js?
        => In using Node.js, the .createServer() method is responsible for creating a server.

    - What method of the response object allows us to set status codes and content types?
        => The .writeHead() method allows us to set status codes and content types on the response object.

    - Where will console.log() output its contents when run in Node.js?
        => It will be displayed on the terminal.

    - What property of the request object contains the address's endpoint?
        = > The .url property contains the address's endpoint.


*/